
/**********************************************************************************************************
/**national 
/**********************************************************************************************************/

option mprint symbolgen;
/********************************************************************/
/****Estimation  de la population prot�g�e RG+SLM qui va nous servir 
/*****de d�nominateur pour la carto des patho
  */
/*****************************************************************/

/***Entr�e des param�tres ***/
* Liste du ou des d�partement entre cote avec virgule si plusieurs;
%LET DEPT ='014','027','050','061','076';
%let ANNEE=2015;



PROC SQL;
DROP TABLE SASDATA1.POP_conso_non_conso_nat;
   CREATE TABLE sasdata1.POP_CONSO_NONCONSO_nat  AS 
   SELECT distinct
	    t1.BEN_SEX_COD,
		case when (&annee-INPUT(t1.BEN_NAI_ANN,4.))<=9	then "[0-9]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=10 and((&annee-INPUT(t1.BEN_NAI_ANN,4.))<20)) then "[10-19]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=20 and ((&annee-INPUT(t1.BEN_NAI_ANN,4.))<30)) then "[20-29]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=30 and ((&annee-INPUT(t1.BEN_NAI_ANN,4.))<40)) then "[30-39]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=40 and  ((&annee-INPUT(t1.BEN_NAI_ANN,4.))<50)) then "[40-49]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=50 and  ((&annee-INPUT(t1.BEN_NAI_ANN,4.))<60)) then "[50-59]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=60 and  ((&annee-INPUT(t1.BEN_NAI_ANN,4.))<70)) then "[60-69]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=70 and  ((&annee-INPUT(t1.BEN_NAI_ANN,4.))<80)) then "[70-79]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=80 and  ((&annee-INPUT(t1.BEN_NAI_ANN,4.))<90)) then "[80-89]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=90  and ((&annee-INPUT(t1.BEN_NAI_ANN,4.))<150))  then "[90+]"
		end as class_age,
		/*t1.BEN_RES_DPT, 
          t1.BEN_RES_COM, */
            (COUNT(DISTINCT(case when t1.BEN_TOP_CNS=1 then t1.BEN_IDT_ANO end))) AS NB_CONSO_REF, /* On se sert du  TOP Conso */
			(COUNT(DISTINCT(t1.BEN_IDT_ANO))) AS NB_TOT_REF
      FROM ORAVUE.IR_BEN_R t1
      WHERE /*t1.BEN_RES_DPT in(&DEPT)
	  AND*/ (SUBSTR(t1.ORG_AFF_BEN,1,3) in('01C'/*,'06A','07A','10A','90A'*/) 
	  				OR (SUBSTR(t1.ORG_AFF_BEN,1,3)='01M' AND SUBSTR(t1.ORG_AFF_BEN,7,3)in('619','537','599','619','516','601','603','604'))
					) 
	 AND t1.BEN_NAI_ANN < '2016' /* Elimination des naissances 2017*/
	 AND (t1.BEN_DCD_AME > '201512' OR t1.BEN_DCD_AME = '160001') /* Elimination  d�c�des ant�rieurs � 2017 en gardant les vivants!!*/
	AND t1.BEN_NAI_ANN not in ('160001')
GROUP BY /*t1.BEN_RES_DPT,
               t1.BEN_RES_COM,*/
			   t1.BEN_SEX_COD,
		class_age
;
QUIT;
